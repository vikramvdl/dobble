import cv2
import numpy as np
import os
from process import threshold
from statistics import median

data_dir = './data/dobble'
files = [ os.path.join(data_dir,f) for f in os.listdir(data_dir) if os.path.isfile(os.path.join(data_dir, f)) ]

def area(contours, ind):
    areas = []

    if (len(ind[0]) == 8):
        return -1

    for i in ind[0]:
        if (i == -1):
            continue

        area = cv2.contourArea(contours[i])
        areas.append(area)

    areas.sort(reverse=True)

    max_ratio = 0
    for i in range(1, len(areas)):
        ratio = areas[i-1]/areas[i]
        if (ratio > max_ratio):
            max_ratio = ratio

    return max_ratio

def get_areas(img):
    _, c, h = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    hierarchy = h[0]
    ind = np.where(hierarchy[:,3] == 0)

    return area(c, ind)

ratios = []
for f in files:
    fn = os.path.splitext(os.path.basename(f))[0]
    if (fn == '.DS_Store'):
        continue
    img = cv2.imread(f)
    bin_img = threshold(img)

    r = get_areas(bin_img)
    if (r == -1):
        continue

    ratios.append(r)

    # cv2.imshow('image', bin_img)
    # cv2.waitKey(1000)

# print(sum(ratios)/len(ratios))
ratios.sort(key=float)
print(median(ratios))
