import os
import cv2
import random
import numpy as np
import math

data_dir = './data/objects'
files = [ os.path.join(data_dir,f) for f in os.listdir(data_dir) if os.path.isfile(os.path.join(data_dir, f)) ]

def generate_rotated_images(fn, destination_folder, n, scale=1):
    im = cv2.imread(fn)

    if (im is None):
        return

    color_value = im[0,0,:]

    for i in range(n):
        angle = random.uniform(0, 360)
        scale = random.uniform(0,2)
        rows,cols,dims = im.shape

        m = cv2.getRotationMatrix2D((cols/2, rows/2), -angle, scale)

        cos = np.abs(m[0, 0])
        sin = np.abs(m[0, 1])

        nW = int((rows * sin) + (cols * cos))
        nH = int((rows * cos) + (cols * sin))

        m[0,2] += nW/2 - cols/2
        m[1,2] += nH/2 - rows/2

        im_rotated = cv2.warpAffine(im, m, (nW, nH), flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT, borderValue=(float(color_value[0]),float(color_value[1]),float(color_value[2])))

        dest_filename = str(i) + '.jpg'
        cv2.imwrite(os.path.join(destination_folder, dest_filename), im_rotated)

for f in files:
    fn = os.path.splitext(os.path.basename(f))[0]
    print(fn)
    directory = os.path.join(data_dir, fn)
    if not os.path.exists(directory):
        os.mkdir(directory)

    generate_rotated_images(f, os.path.join(data_dir,fn), 100)
    # im = cv2.imread(f)
