import numpy as np
import matplotlib.pyplot as plt
import cv2

# Construct some test data
# x, y = np.ogrid[-np.pi:np.pi:100j, -np.pi:np.pi:100j]
# r = np.sin(np.exp((np.sin(x)**3 + np.cos(y)**2)))
img1 = cv2.imread('/Users/vikram.vedala/Documents/practice/dobble/data/im3.jpg')
img2 = cv2.imread('/Users/vikram.vedala/Documents/practice/dobble/data/im2.jpg')

img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
img2 = cv2.resize(img2, (640,640))
img1 = img1

img1 = cv2.blur(img1, (5,5))
cv2.imshow('image', img1)
cv2.waitKey(0)

v, bin_img = cv2.threshold(img1, 200, 255, cv2.THRESH_BINARY)
print(v)
cv2.imshow('image', bin_img)
cv2.waitKey(0)

con_img, c, h = cv2.findContours(bin_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
hierarchy = h[0]
print(len(c), hierarchy)
ind = np.where(hierarchy[:,3] == 0)
print(ind)

for i in ind[0]:
# for i in range(len(c)):
    if (i == -1):
        continue
    print(i, hierarchy[i])
    area = cv2.contourArea(c[i])
    print(area)
    if (area < 300):
        continue
    im = con_img.copy()
    c_img = cv2.drawContours(im, c, i, (255,255,255), cv2.FILLED, cv2.LINE_8)
    cv2.imshow('image',c_img)
    cv2.waitKey(0)

