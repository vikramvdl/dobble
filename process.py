import numpy as np
import matplotlib.pyplot as plt
import cv2
import time
from label_image import label_image

AREA_RATIO_THRESHOLD = 24
THRESHOLD = 200
SHAPE = (640,640)

def threshold(img):
	img1 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

	img1 = cv2.blur(img1, (3,3))
	_, bin_img = cv2.threshold(img1, THRESHOLD, 255, cv2.THRESH_BINARY)
	return bin_img

def filter_small_contours(contours, ind, hierarchy):
	c = []
	h = []
	areas = np.ones((len(ind[0]), 2))
	counter = 0
	for i in ind[0]:
		if (i == -1):
			continue

		area = cv2.contourArea(contours[i])
		areas[counter,:] = [area, i]
		counter = counter + 1

	areas = areas[areas[:,0].argsort()][::-1]

	index = -1
	for i in range(1, len(areas)):
		r = areas[i-1,0]/areas[i,0]
		if (r > AREA_RATIO_THRESHOLD):
			index = i-1
			break

	if (index == -1):
		c = [contours[i] for i in ind[0]]
		h = [hierarchy[i] for i in ind[0]]
	else:
		c = [contours[int(i)] for i in areas[:index+1,1]]
		h = [hierarchy[int(i)] for i in areas[:index+1,1]]

	# i = np.where(areas[:,0] < AREA_RATIO_THRESHOLD)
	return c, h

def find_objects(img):
	con_img, c, h = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
	hierarchy = h[0]
	ind = np.where(hierarchy[:,3] == 0)

	contours, hierarchy = filter_small_contours(c, ind, hierarchy)
	return ind, con_img, contours, hierarchy

def retrieve_top_matches(c1, c2, n):
	match = 100
	result = -1*np.ones(((len(c1))*(len(c2)),3))

	for i in range(len(c1)):
		for j in range(len(c2)):
			ret = cv2.matchShapes(c1[i], c2[j], cv2.CONTOURS_MATCH_I3, 0.0)
			result[i*len(c2)+j] = [ret, i, j]

	result = result[result[:,0].argsort()]
	return result[:n]

def draw_objects(img, c):
	i = img.copy()
	c_img = cv2.drawContours(i, c, -1, (255,120,20), 4, cv2.LINE_8)
	cv2.imshow('image', c_img)
	cv2.waitKey(0)

def get_bounding_box(im, c):
	x1=np.max(c,0)[[0][0]][0]
	y1=np.max(c,0)[[0][0]][1]
	x2=np.min(c, 0)[[0][0]][0]
	y2=np.min(c, 0)[[0][0]][1]
	# print(x1,y1,x2,y2)
	padding = 15
	obj = im[np.min([y1,y2])-padding:np.max([y1,y2])+padding, np.min([x1,x2])-padding:np.max([x1,x2])+padding :]
	return obj

def compare_pics(im1, im2, c1, c2, color, thickness, line_type, label=''):
	c_img = cv2.drawContours(im1, c1, -1, color, thickness, line_type)
	plt.subplot(1,2,1)
	plt.imshow(cv2.cvtColor(c_img, cv2.COLOR_BGR2RGB))

	c_img = cv2.drawContours(im2, c2, -1, color, thickness, line_type)
	plt.subplot(1,2,2)
	plt.imshow(cv2.cvtColor(c_img, cv2.COLOR_BGR2RGB))
	if (label is not ''):
		plt.suptitle("Detected Match: " + label)
	plt.show()

if __name__ == "__main__":
	start = time.time()
	img1 = cv2.imread('./data/dobble/8392137577_000c61d6e0_z.jpg')
	img2 = cv2.imread('./data/dobble/8392138359_b724377573_z.jpg')

	if (np.shape(img1) is not SHAPE):
		img1 = cv2.resize(img1, SHAPE)
	if (np.shape(img2) is not SHAPE):
		img2 = cv2.resize(img2, SHAPE)

	bin_img1 = threshold(img1)
	bin_img2 = threshold(img2)

	ind1, c_img1, c1, h1 = find_objects(bin_img1)
	ind2, c_img2, c2, h2 = find_objects(bin_img2)

	# draw_objects(img1, c1)
	# draw_objects(img2, c2)

	result = retrieve_top_matches(c1, c2, 10)
	# print(result)

	for r in result:
		# print(r)
		match_c1 = c1[int(r[1])]

		obj1 = get_bounding_box(img1, match_c1)
		# plt.show()
		# cv2.imwrite('first_match.jpg',obj1)

		label1, sc1 = label_image(obj1)

		im1 = np.copy(img1)

		# cv2.imshow('image', obj1)
		# cv2.waitKey(0)
		match_c2 = c2[int(r[2])]
		im2 = np.copy(img2)

		obj2 = get_bounding_box(img2, match_c2)

		label2, sc2 = label_image(obj2)
		# compare_pics(im1, im2, match_c1, match_c2, (255,120,20), 6, cv2.LINE_8)

		if (label1 == label2):
			end = time.time()
			print("Elapsed time: ", end-start)
			print("Match found: ", label1)
			compare_pics(im1, im2, match_c1, match_c2, (255,120,20), 6, cv2.LINE_8, label1)
			break
